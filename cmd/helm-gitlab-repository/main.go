package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/go/helm-gitlab-repository/lib/helm"
	"gitlab.com/shadowy/go/helm-gitlab-repository/lib/repository"
	"gitlab.com/shadowy/go/helm-gitlab-repository/lib/util"
	"os"
	"time"
)

var cfg = []util.AppVariable{
	{Name: "gitlab-token", Env: "GITLAB_TOKEN", Default: "", Description: "gitlab token"},
	{Name: "namespace", Env: "NAMESPACE", Default: "", Description: "git namespace"},
	{Name: "file", Env: "FILE", Default: "values.yaml", Description: "helm values file"},
}

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339Nano})
	log.Logger.Info().Msg("helm-gitlab-repository")
	cfg, err := util.AppVariables(cfg)
	if err != nil {
		log.Logger.Error().Err(err).Msg("helm-gitlab-repository")
		return
	}
	file, err := helm.ValuesFile{}.Read(cfg["file"])
	if err != nil {
		return
	}
	rep, err := repository.GetRepository(cfg["gitlab-token"])
	if err != nil {
		return
	}
	list, err := rep.GetImages(cfg["namespace"])
	if err != nil {
		return
	}
	file.UpdateServiceToLatest(list)
	_ = file.Save()
}
