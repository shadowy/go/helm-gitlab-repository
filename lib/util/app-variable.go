package util

import (
	"errors"
	"flag"
	"os"
)

type AppVariable struct {
	Name        string
	Env         string
	Default     string
	Description string
}

func AppVariables(list []AppVariable) (map[string]string, error) {
	v := map[AppVariable]*string{}
	for i := range list {
		item := list[i]
		v[item] = flag.String(item.Name, item.Default, item.Description)
	}
	flag.Parse()
	res := map[string]string{}
	for i := range v {
		val := v[i]
		if val != nil && *val != "" {
			res[i.Name] = *val
			continue
		}
		s := os.Getenv(i.Env)
		if s == "" {
			return nil, errors.New(i.Name + " or environment variable " + i.Env + " not defined")
		}
		res[i.Name] = s
	}
	return res, nil
}
