package repository

import (
	"github.com/rs/zerolog/log"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/shadowy/go/helm-gitlab-repository/lib/repository/model"
	"gitlab.com/shadowy/go/utils"
	"strings"
)

type Repository struct {
	client *gitlab.Client
}

func GetRepository(token string) (*Repository, error) {
	log.Logger.Debug().Msg("GetRepository")
	res := new(Repository)
	var err error
	res.client, err = gitlab.NewClient(token)
	if err != nil {
		log.Logger.Error().Err(err).Msg("GetRepository")
		return nil, err
	}
	return res, nil
}

func (r *Repository) GetImages(namespace string) (map[string][]string, error) {
	log.Logger.Debug().Msg("Registry.GetImages")
	var projects []*gitlab.Project
	page := 0
	for {
		temp, _, err := r.client.Projects.ListProjects(&gitlab.ListProjectsOptions{
			ListOptions: gitlab.ListOptions{
				Page:    page,
				PerPage: 100,
			},
			Search:           utils.PtrString(namespace),
			SearchNamespaces: utils.PtrBool(true),
			Membership:       utils.PtrBool(true),
			OrderBy:          utils.PtrString("path"),
			Sort:             utils.PtrString("asc"),
		})
		if err != nil {
			log.Logger.Error().Err(err).Msg("Registry.GetImages ListUserProjects")
			return nil, err
		}
		if len(temp) == 0 {
			break
		}
		page++
		projects = append(projects, temp...)
	}
	var list []*model.Project
	for i := range projects {
		p := projects[i]
		res := model.Project{}.FromGitlab(p)
		list = append(list, res)
		log.Logger.Debug().Int("projectID", p.ID).Str("path", p.PathWithNamespace).Msg("Registry.GetImages ListRegistryRepositories")
		reps, _, err := r.client.ContainerRegistry.ListRegistryRepositories(p.ID, &gitlab.ListRegistryRepositoriesOptions{
			Tags: utils.PtrBool(true),
		})
		if err != nil {
			log.Logger.Error().Err(err).Int("projectID", p.ID).Msg("Registry.GetImages ListRegistryRepositories")
			return nil, err
		}
		for k := range reps {
			res.AddRegistry(reps[k])
		}
	}
	res := map[string][]string{}
	for i := range list {
		p := list[i]
		for k := range p.List {
			r := p.List[k]
			res[strings.ToLower(r.Location)] = r.Tags
		}
	}
	return res, nil
}
