package model

import "github.com/xanzy/go-gitlab"

type Registry struct {
	ID       int
	Location string
	Tags     []string
}

func (r Registry) FromGitlab(registry *gitlab.RegistryRepository) *Registry {
	res := &Registry{
		ID:       registry.ID,
		Location: registry.Location,
	}
	for i := range registry.Tags {
		res.Tags = append(res.Tags, registry.Tags[i].Name)
	}
	return res
}

func (r *Registry) Latest() string {
	return r.Tags[len(r.Tags)-1]
}
