package model

import "github.com/xanzy/go-gitlab"

type Project struct {
	ID   int
	Path string
	Name string
	List []*Registry
}

func (p Project) FromGitlab(project *gitlab.Project) *Project {
	return &Project{
		ID:   project.ID,
		Path: project.PathWithNamespace,
		Name: project.Name,
	}
}

func (p *Project) AddRegistry(registry *gitlab.RegistryRepository) {
	p.List = append(p.List, Registry{}.FromGitlab(registry))
}
