package helm

import (
	"errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v3"
	"os"
	"strings"
)

type ValuesFile struct {
	l    zerolog.Logger
	file string
	data map[string]interface{}
}

func (v ValuesFile) Read(file string) (*ValuesFile, error) {
	log.Logger.Debug().Str("file", file).Msg("ValuesFile.Read")
	res := new(ValuesFile)
	log.Logger.With().Logger()
	err := res.Load(file)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (v *ValuesFile) Load(file string) error {
	v.l = log.Logger.With().Str("file", file).Logger()
	v.l.Debug().Msg("ValuesFile.Load")
	v.file = file
	data, err := os.ReadFile(file)
	if err != nil {
		v.l.Error().Err(err).Msg("ValuesFile.Read ReadFile")
		return err
	}
	err = yaml.Unmarshal(data, &v.data)
	if err != nil {
		v.l.Error().Err(err).Msg("ValuesFile.Read Unmarshal")
		return err
	}
	if _, ok := v.data["services"]; !ok {
		v.l.Error().Msg("ValuesFile.Read services not defined")
		return errors.New("ValuesFile.Read services not defined")
	}
	return nil
}

func (v *ValuesFile) UpdateServiceToLatest(repository map[string][]string) {
	v.l.Debug().Msg("ValuesFile.UpdateServiceToLatest")
	services := v.data["services"].([]interface{})
	for i := range services {
		s := services[i].(map[string]interface{})
		name := s["name"].(string)
		image := s["image"].(string)
		version := s["version"].(string)
		if tags, ok := repository[strings.ToLower(image)]; ok {
			newVersion := tags[len(tags)-1]
			if newVersion == version {
				continue
			}
			s["version"] = version
			v.l.Debug().
				Str("name", name).
				Str("image", image).
				Str("version", version).
				Str("newVersion", newVersion).
				Msg("UpdateServiceToLatest service")
		} else {
			v.l.Warn().
				Str("name", name).
				Str("image", image).
				Str("version", version).
				Msg("UpdateServiceToLatest service not found image")
		}
	}
}

func (v *ValuesFile) Save() error {
	v.l.Debug().Msg("ValuesFile.Save")
	data, err := yaml.Marshal(v.data)
	if err != nil {
		v.l.Error().Err(err).Msg("ValuesFile.Save Marshal")
		return err
	}
	err = os.WriteFile(v.file, data, 0x666)
	if err != nil {
		v.l.Error().Err(err).Msg("ValuesFile.Save WriteFile")
		return err
	}
	return nil
}
